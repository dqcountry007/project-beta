from django.contrib import admin
from .models import Appointment, AutomobileVO, Tech


admin.site.register(AutomobileVO)
admin.site.register(Appointment)
admin.site.register(Tech)

