# APIs

## Home page
* **Method**: `GET`
* **Path**: /
* returns home page of the app. 

## Create a technician
* **Method**: `POST`
* **Path**: /tech
* create/enroll a technician form page

Input:
```json
{
    "name": string,
    "id": int
}
```

Output:
```json
{
    "href": url,
    "name": string,
    "id": int
}
```
## Create an appointment
* **Method**: `POST`
* **Path**: /appointment
* create/book an appointment for your automobile

Input:
```json
{
    "tech": int,
    "owner": str,
    "date": DateTime,
    "time": TimeField,
    "automobile": {
        automobile obj,
    },
    "reason": str,
    "finished": bool,
    "cancelled": bool,
    "vip": bool
}
```

Output:
```json
{
    "id": int,
    "tech": int,
    "owner": str,
    "date": DateTime,
    "time": TimeField,
    "automobile": {
        automobile obj,
    },
    "reason": str,
    "finished": bool,
    "cancelled": bool,
    "vip": bool
}
```

## Create a manufacturer
* **Method**: `POST`
* **Path**: /createManufacturer
* create a manufacturer to associate an automobile with.

Input:
```json
{
	"name": str
}
```
Output:
```json
{
	"href": url,
	"id": int,
	"name": str
}
```

## List all vehicle models
* **Method**: `GET`
* **Path**: /modelList
* List all vehicle models in inventory

Output:
```json
{
	"models": [
		{
			"href": url,
			"id": int,
			"name": str,
			"picture_url": url,
			"manufacturer": {
				"href": url,
				"id": int,
				"name": str
			}
    ]
}
```

## List all automobiles
* **Method**: `GET`
* **Path**: /autoList
* List all automobiles in inventory

Output:
```json
{
	"autos": [
		{
			"href": url,
			"id": int,
			"color": str,
			"year": int,
			"vin": str,
			"model": {
				"href": url,
				"id": int,
				"name": str,
				"picture_url": url,
				"manufacturer": {
					"href": url,
					"id": int,
					"name": str
				}
			}
		}
    ]
}
```

## Create an automobile
* **Method**: `POST`
* **Path**: /createAuto
* Create an automobile using manufacturer, model, vin, color, year

Input:
```json
{
  "color": str,
  "year": int,
  "vin": str,
  "model_id": int
}
```

Output:
```json
{
	"href": url,
	"id": int,
	"color": str,
	"year": int,
	"vin": str,
	"model": {
		"href": url,
		"id": int,
		"name": str,
		"picture_url": url,
		"manufacturer": {
			"href": url,
			"id": int,
			"name": str
		}
	}
}
```


## List all manufacturers
* **Method**: `GET`
* **Path**: /manufacturer
* List all automobile manufacturers in inventory

Output:
```json
{
	"manufacturers": [
		{
			"href": url,
			"id": int,
			"name": str
		},
    ]
}
```

## Create a vehicle model
* **Method**: `POST`
* **Path**: /createModel
* Create a vehicle model to store in inventory

Input:
```json
{
  "name": str,
  "picture_url": url,
  "manufacturer_id": int
}
```

Output:
```json
{
	"href": url,
	"id": int,
	"name": str,
	"picture_url": url,
	"manufacturer": {
		"href": url,
		"id": int,
		"name": str
	}
}
```


## Create/add a sales person
* **Method**: `POST`
* **Path**: /addSalesPerson
* Create a sales person that works for the dealership

Input:
```json
{
	"name": str,
	"id": int
}
```

Output:
```json
{
	"href": url,
	"name": str,
	"id": int
}
```


## Create/add a customer
* **Method**: `POST`
* **Path**: /addCustomer
* Create/add a customer that will be buying an automobile

Input:
```json
{
	"name": str,
	"address": str,
	"phone": str
}
```

Output:
```json
{
	"href": url,
	"name": str,
	"address": str,
	"phone": str,
	"id": int
}
```

## Record a sale
* **Method**: `POST`
* **Path**: /recordSale
* Create a sale record of an automobile, associating a sales person, automobile, customer, and price of vehicle. 

Input:
```json
{ 
	"automobile": str,
	"sales_person": int,
	"customer": int,
	"price": int
}
```

Output:
```json
{
	"id": int,
	"sales_person": {
		"href": url,
		"name": str,
		"id": int
	},
	"automobile": {
		"color": str,
		"year": int,
		"vin": str,
		"sold": bool
	},
	"customer": {
		"href": url,
		"name": str,
		"address": str,
		"phone": str,
		"id": int
	},
	"price": int
}
```


## List all sales
* **Method**: `GET`
* **Path**: /listSales
* List all of the sales the dealership has made 

Output:
```json
{
	"sales": [
		{
			"id": int,
			"sales_person": {
				"href": url,
				"name": str,
				"id": int
			},
			"automobile": {
				"color": str,
				"year": int,
				"vin": str
			},
			"customer": {
				"href":url,
				"name": str,
				"address": str,
				"phone": str,
				"id": int
			},
			"price": int
		},
    ]
}
```

## Sales person history
* **Method**: `GET`
* **Path**: /salespersonHistory
* List all of the sales made by a specific salesperson

Output:
```json
[
	{
		"id": int,
		"sales_person": {
			"href": url,
			"name": str,
			"id": int
		},
		"automobile": {
			"color": str,
			"year":int,
			"vin": str
		},
		"customer": {
			"href": url,
			"name": str,
			"address": str,
			"phone": str,
			"id": int
		},
		"price": int
	}
]
```

## List all of the automobile service appointments
* **Method**: `GET`
* **Path**: /appointmentList
* List all of the appointments made for automobiles in the Service side of the dealership. 

Output:
```json
{
    "appointments": {
        "vin": int,
        "owner": {
            "id": int,
            "name": str,
            "address": str,
            "phone": str
        },
        "reason": str,
        "tech": {
            "id": int,
            "name": str
        },
        "date": DateTime,
        "time": Timefield,
        "vip": bool
    }
}
```

## Search list 
* **Method**: `GET`
* **Path**: /searchList
* Search for the records of an automobile based on its VIN number. 

Output:
```json
{
    "search": {
        "vin": int,
        "customer": {
            "name": str,
            "address": str,
            "phone": str,
            "id": int
        },
        "date": DateTime,
        "time": Timefield,
        "tech": {
            "id": int,
            "name": str
        },
        "reason": str
    }
}
```


