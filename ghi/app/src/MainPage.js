import './index.css';

function MainPage() {
  return (
    <div className="px-4 py-5 text-center" id="mainContainer">
      <h1 className="display-5 fw-bold" id="mainHeading">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4" id="slogan">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
  );
}

export default MainPage;