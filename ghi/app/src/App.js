import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './TechForm';
import AppointmentForm from './AppointmentForm';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
import ManufacturerList from './ManufacturerList';
import VehicleForm from './VehicleForm';
import AddSalesPersonForm from './AddSalesPerson.js';
import AddCustomerForm from './AddCustomer.js';
import SaleRecordForm from './CreateSaleRecord.js';
import SaleList from './ListSales.js';
import SalesPersonHistory from './SalesPersonHistory.js';
import AppointmentList from './AppointmentList';
import SearchList from './SearchVin';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/tech" element={<TechForm />} />
          <Route path="/appointment" element={<AppointmentForm />} />
          <Route path="/createManufacturer" element={<ManufacturerForm />} />
          <Route path="/modelList" element={<ModelList />} />
          <Route path="/autoList" element={<AutoList />} />
          <Route path="/createAuto" element={<AutoForm />} />
          <Route path="/manufacturer" element={<ManufacturerList />} />
          <Route path="/createModel" element={<VehicleForm />} />
          <Route path="/addSalesPerson" element={<AddSalesPersonForm />} />
          <Route path="/addCustomer" element={<AddCustomerForm />} />
          <Route path="/recordSale" element={<SaleRecordForm />} />
          <Route path="/listSales" element={<SaleList />} />
          <Route path="/salespersonHistory" element={<SalesPersonHistory />} />
          <Route path="/appointmentList" element={<AppointmentList />} />
          <Route path="/searchList" element={<SearchList />} /> 
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
