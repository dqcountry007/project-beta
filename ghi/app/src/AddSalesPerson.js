import React from 'react';

class AddSalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            id: '',
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleIdChange = this.handleIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {...this.state};
        
        //POST url for create sales person
        const salesPersonUrl = `http://localhost:8090/api/sales/person/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesPersonUrl, fetchConfig);

        if (response.ok) {
            const newSalesPerson = await response.json();

            const cleared = {
                name: '',
                id: '',
            };
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleIdChange(event) {
        const value = event.target.value;
        this.setState({id: value})
    }

    render() {
        return(
            <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Add a new sales person</h1>
                            <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                    <label htmlFor="name">Name</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleIdChange} value={this.state.id} placeholder="Id" required type="text" name="id" id="id" className="form-control" />
                                    <label htmlFor="id">Sales Person Id</label>                     
                                </div>                   
                                <button className="btn btn-primary" id="salesPersBtn">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
        );
    }
}

export default AddSalesPersonForm;