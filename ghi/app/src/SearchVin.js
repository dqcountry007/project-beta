import React from 'react';

class SearchList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filteredAppointments: [],
        };
    this.handleSearch = this.handleSearch.bind(this)
    }
    async handleSearch(event) {
        const value = event.target.value
        const searchUrl = `http://localhost:8080/api/vin/appointment/${value}/`
        const searchResponse = await fetch(searchUrl)

        if(searchResponse.ok){
            const searchData = await searchResponse.json()
            
            this.setState({filteredAppointments: searchData})
        }
    }

    async componentDidMount() {
        const appointmentUrl = 'http://localhost:8080/api/appointment/';
        const appointmentResponse = await fetch(appointmentUrl)
        if (appointmentResponse.ok){
            const appointmentData = await appointmentResponse.json()
            
            this.setState({appointments: appointmentData})
        }

    }
    render() {
        return (
            <>
        <div className="container">

        <div className="row height d-flex justify-content-center align-items-center">

        <div className="col-md-6">

            <div className="form">
            <i className="fa fa-search"></i>
            <input onChange={this.handleSearch} type="text" className="form-control form-input my-5" placeholder="Search by VIN#" id="searchBar" />
            <span className="left-pan"><i className="fa fa-microphone"></i></span>
            </div>
            
        </div>
        
        </div>

        </div>

      <table className="table table-striped my-5">
        <thead>
            <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
            </tr>
        </thead>
        <tbody>
            {this.state.filteredAppointments.map(appoint => {
              console.log("appointment is: ", appoint);
                return (
                <tr key={appoint.id}>
                    <td id="appointData">{ appoint.automobile.vin }</td>
                    <td id="appointData">{ appoint.owner }</td>
                    <td id="appointData">{ appoint.date }</td>
                    <td id="appointData">{ appoint.time }</td>
                    <td id="appointData">{ appoint.tech.name }</td>
                    <td id="appointData">{ appoint.reason }</td>
                </tr>
                );
            })}  
        </tbody>
    </table>
    </>
    );
  }
}



// value={this.state.appointment.automobile.vin}


export default SearchList;


