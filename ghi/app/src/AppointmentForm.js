import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            techs: [],
            owner: '',
            reason: '',
            date: '',
            time: '',
            vip: '',
            automobiles: []
        };

        this.handleTechChange = this.handleTechChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleVipChange = this.handleVipChange.bind(this);
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleTechChange(event) {
        const value = event.target.value;
        this.setState({tech: value})
    }

    handleOwnerChange(event) {
        const value = event.target.value;
        this.setState({owner: value})
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value})
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({date: value})
    }

    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({time: value})
    }

    handleVipChange(event) {
        const value = event.target.value;
        this.setState({vip: value})
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile: value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state, finished: "False", canceled: "False"};
        delete data.automobiles
        delete data.techs
        
        const appointmentUrl = `http://localhost:8080/api/appointment/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        console.log("data is: ", data);
        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            const newAppointment = await response.json();

            const cleared = {
                techs: [],
                owner: '',
                reason: '',
                date: '',
                time: '',
                vip: '',
                automobiles: []
                };
            this.setState(cleared);
        }
    }


    async componentDidMount() {

        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        const techUrl = 'http://localhost:8080/api/tech/'
        const techResponse = await fetch(techUrl)

        if (response.ok) {
            const data = await response.json();
            const techData = await techResponse.json()
            // console.log("data is: ", techData);
          
            this.setState({automobiles: data.autos})
            this.setState({techs: techData.tech})
        }
    }

    render() {
        return(
            <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1 id="appHeader">Create a new Appointment</h1>
                            <form onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="mb-3">
                                    <select onChange={this.handleTechChange} value={this.state.tech} required id="tech" name="tech" className="form-select">
                                        <option value="">Choose your Tech</option>
                                        {this.state.techs.map(tech => {
                                            return (
                                                <option key={tech.id} value={tech.id}>{tech.name}</option>
                                            );
                                        })}
                                    </select>
                                  </div>     
                                {/* <div className="form-floating mb-3">
                                    <input onChange={this.handleTechChange} value={this.state.tech} placeholder="Technician ID" required type="text" name="technician_id" id="technician_id" className="form-control" />
                                    <label htmlFor="tech">Technician ID</label>                     
                                </div> */}
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleOwnerChange} value={this.state.owner} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control" />
                                    <label htmlFor="owner">Vehicle Owner</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                    <label htmlFor="reason">Reason for Appointment</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleDateChange} value={this.state.date} placeholder="Date" required type="text" name="date" id="date" className="form-control" />
                                    <label htmlFor="date">YYYY-MM-DD</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleTimeChange} value={this.state.time} placeholder="Time" required type="text" name="time" id="time" className="form-control" />
                                    <label htmlFor="time">00:00</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleVipChange} value={this.state.vip} placeholder="Vip" required type="text" name="vip" id="vip" className="form-control" />
                                    <label htmlFor="vip">Vehicle purchased from this Dealership? ("True" or "False")</label>                     
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleAutomobileChange} value={this.state.automobile} required id="automobile" name="automobile" className="form-select">
                                        <option value="">Choose the Vehicle</option>
                                        {this.state.automobiles.map(automobile => {
                                            return (
                                                <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                            );
                                        })}
                                    </select>
                                  </div>                    
                                <button className="btn btn-primary" id="appBtn">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
        );
    }
}

export default AppointmentForm;