import React from 'react';
import './index.css';

function ModelColumn(props) {
    return (
        <div className="col">
            {props.list.map(car => {
                return (
                    <div key={car.href} className="card mb-3 shadow">
                        <img src={car.picture_url} className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{car.name}</h5>
                            <h6 className="card-subtitle mb-2">
                                {car.manufacturer.name}
                            </h6>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

class ModelList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            carColumns: [[], [], []],
        };
    }


    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';

        try {
            const response = await fetch(url);
            if (response.ok) {
    
                const data = await response.json();
                const requests = [];
                for (let car of data.models) {
                    const detailUrl = `http://localhost:8100/api/models/${car.id}`;
                    
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const carColumns = [[], [], []];
                let i = 0;
                for (const carResponse of responses) {
                    if (carResponse.ok) {
                        const details = await carResponse.json();
                        carColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(carResponse);
                    }
                }

                this.setState({ carColumns: carColumns });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center" id="vehicles">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Vehicle Models</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4" id="vehModSub">
                            Find a vehicle model that fits your style.
                        </p>
                    </div>
                </div>
                <div className="container" id="vehicleModelContainer">
                    <h3 id="scrollList">Please scroll to see all options</h3>
                    <div className="row">
                        {this.state.carColumns.map((carList, index) => {
                            return (
                                <ModelColumn key={index} list={carList} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default ModelList;
