# Application name: CarCar

## Summary:
* Automobile dealership management system

## Design
* [API Design](docs/apis.md)
* [Data model](docs/data-model.md)

## Team:
* Brandon Dix - Service/Inventory
* David Quinlan - Sales/Inventory

## Technologies:
* HTML/CSS
* Python
* Django
* JavaScript
* React
* PostgreSQL
* Bootstrap
* RESTful APIs
* Docker
* microservices 


## Functionality:
* Inventory 
   * A user can create a manufacturer, vehicle model, and automobile with a VIN number for a specific model to store in the system. 
   * List all vehicle models including the manufacturer name, model name, and an image associated with the model. 
   * List all automobiles currently in inventory, including details such as VIN, year, color, model, and manufacturer. 

* Sales 
   * Create/enroll a salesperson and customer to associate a sale of an automobile with. 
   * Record a sale for a specific automobile with the price, by a specific sales person, for a particular customer. 
   * Ability to see all the sales the dealership has done 
   * View past sales for a specific sales person 

* Services
   * Create/enroll a technician to associate the services for an automobile
   * Schedule an appointment for an automobile, including a "VIP" status, where a user can specify if they bought the vehicle from the dealership. 
   * See all of the upcoming appointments for the dealership 
   * View past appointments for an automobile based off the VIN number. 



## Screenshots

### CarCar Home page
* ![Home_page](docs/screenshots/CarCar_home_page.png)

### Inventory microservice

* ![Create_a_Manufacturer](docs/screenshots/Inventory_Create_Manufacturer.png)
* ![Create_a_Vehicle_Model](docs/screenshots/Inventory_Create_Vehicle_Model.png)
* ![Create_an_Automobile](docs/screenshots/Inventory_Create_Automobile.png)
* ![List_all_Manufacturers](docs/screenshots/Inventory_List_Manufacturers.png)
* ![List_all_VehicleModels_1](docs/screenshots/Inventory_VehicleModels_1.png)
* ![List_all_VehicleModels_2](docs/screenshots/Inventory_VehicleModels_2.png)
* ![List_all_VehicleModels_3](docs/screenshots/Inventory_VehicleModels_3.png)
* ![List_all_VehicleModels_4](docs/screenshots/Inventory_VehicleModels_4.png)
* ![List_all_Automobiles](docs/screenshots/Inventory_List_Automobiles.png)

### Sales microservice

* ![Add_a_Salesperson](docs/screenshots/Sales_Add_Salesperson_form.png)
* ![Add_a_Customer](docs/screenshots/Sales_Add_Customer_form.png)
* ![Record_a_Sale](docs/screenshots/Sales_Record_a_Sale.png)
* ![List_all_Sales](docs/screenshots/Sales_List_All_Sales.png)
* ![See_a_Salespersons_History](docs/screenshots/Sales_Salesperson_History.png)

### Services microservice
* ![Enroll_a_Technician](docs/screenshots/Services_Employee_Enrollment.png)
* ![Make_an_Appointment](docs/screenshots/Services_Create_New_Appointment.png)
* ![List_all_Appointments](docs/screenshots/Services_AppointmentList.png)
* ![Appointment_History_based_on_VIN_Search](docs/screenshots/Services_AppointmentHistory.png)


## How to clone and run this application
   ### Requirements: 
   * Docker desktop installed 

* 1.) Visit the CarCar repository at https://gitlab.com/dqcountry007/project-beta
* 2.) Click on the button labeled, "Clone" that has the dropdown icon next to it.
* 3.) Copy the url under the "Clone with HTTPS".
* 4.) Open your terminal and change into a directory where you want to store this application.
* 5.) In your terminal, type "git clone" then paste the HTTPS url you copied and hit enter on your keyboard.
* 6.) Now the CarCar repository is cloned locally to your machine.
* 7.) Change directories into the CarCar directory. 
* 8.) You will need to open the Docker desktop application. 
* 9.) In your terminal, in the CarCar directory, type "docker volume create beta-data" to create the volume to store your data. Think of a volume like a "thumb drive".  
* 10.) Next in your terminal, type "docker-compose build" to build the Docker image, which is essentially a "hard drive" for the 
virtual computer that we will get running next. 
* 11.) Next, type "docker-compose up" to get the containers/app running. Containers are the actual virtual computer, which is an environment where you can develop your application. 
* 12.) In your browser, go to http://localhost:3000 to get to the CarCar homepage. 


## Service microservice
AutomobileVo Model:
 - color
 - year
 - vin

Appointment Model:
 - Automobile (foreign key to Automobile Model)
 - Owner
 - Date and Time of appointment
 - Tech object
 - Reason for service appointment
 - Automobile object
 - finished
 - canceled
 - vip

Tech Model:
 - name
 - id

Bounded Context: (Service) / (Sales) / (Inventory)

Looking forward the services will have more to implement within that context that will add more reason for it to be separated and same with sales, I imagine sales will have to handle payment and those sorts of things in the future.

Aggregate root: Dealership - { services, sales, inventory }

Aggregates: 

Service:
 - Entities:
    - Appointments 
    - Technicians
 - Value Objects:
    - AutomobileVO is a value object 

Inventory: 
 - Entities:
    - Automobile 
 - Value Objects:
    - Manufacturer
    - Vehicle Models

Sales:
 - Entities:
    - Sales Person
    - Sales Record
    - Customer
 - Value Objects:
    - AutomobileVO


Aggregate Root Service:

This service will be polling from the inventory microservice to obtain the Automobile model data. We will use that objects VIN property to uniquely identify the vehicle being serviced within the service appointment system. We are creating two separate models because we want data tables for the technician and the appointment to be separate. This microservice is designed to control the data flow/display of appointments made by a service employee. It takes data requests via form to schedule an appointment within our server. We assign a tech at this time, then we display the appointments in a list so our employees can have access to all the necessary properties of the appointment instance. We can also filter out that data based on the VIN of the vehicle being serviced and display the history of the vehicles services. When an employee is looking at a service appointment they can interact with the data by submitting a "finished" or "canceled" "put" request back to our database to change that boolean field to either True for "finished" or "canceled". This will be something our dev team can see but the requirements of this project in my opinion, should have had this displayed in the service history list. *This will be a change I implement going forward. The technician data flow is simply to create a technician with their ID in our system and to assign then to appointments.

Appointment is an Entity because it can be continuously identified and it also an object that goes through a life cycle decided either by "finished" status, "canceled" status or removal from the database

Technician is an Entity because it can be continuously identified and it also an object that goes through a life cycle decided either by removal from the database, (i.e. fired/quit)

Automobile is a Value Object as described by its name. It is a value object created in our respective micro services to pull the necessary data from the Automobile Object with in the inventory bounded context.

Takeaway:

 - Planning is important. Really read through the requirements and make a plan before you are coding. 
 - It is easy to crete your own functions that can help towards a desired goal.


## Sales microservice

For the sales microservice, it required me to be able to add a sales person, add a customer, record a sale, list all sales, and get a specific sales person's history. Knowing these requirements, I knew that in order to do anything in sales would require automobile information from the Inventory API. Having automobile information allows me to create a sales record, list all sales, and get specific sales information for a sales person. By creating an Automobile value object model in the Sales application, I could then poll the Inventory API for all of its automobile data and use it in the Sales microservice. 

By identifying Inventory, Sales, and Services as the aggregate root it provided a better understanding of how to proceed. For the Sales microservice, the AutomobileVO, SalesPerson, PotentialCustomer, and SalesRecord models were going to be the aggregates, because all of these in different combinations allowed me to use methods on the Sales aggregate root, such as creating a sale, listing a sale, etc. I also identified SalesPerson and PotentialCustomer as entities, because while they have similar properties (i.e: name), they are not the same thing. They have continuous identity because even though the salesperson and customer could change their name, they are still the same salesperson and the same customer. SalesRecord would be identified as a value object, because there is no life cycle on it. It simply exists with the recorded information.

A few decisions I was faced with when building this program, was the multitude of links that we had going on required us to research an effective navigation bar. Putting the links with their respective dropdown category helped keep things clear and concise from both a user and developer perspective. While most of our "list" pages use cards to display the material, I decided that putting sales person data into a traditional table would be a more effective route for displaying the information, since there was quite a bit of info to display. A big challenge was getting the "Record a new sale" page to have the three dropdown menus and putting all of that code into the componentDidMount and formatting it correctly. Overall, I have learned a lot more about the way React works with the different components and have become more familiar with the specific quirks and errors that occur. 











